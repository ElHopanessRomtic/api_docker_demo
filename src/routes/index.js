const router = require('express').Router()

module.exports = (db) => {
  router.get('/', (req, res, next) => {
    // return next({
    //   statusCode: 401,
    //   error: new Error('Take my hand'),
    // })

    res.removeHeader('X-Powered-By')
    res.json({
      success: true,
      data: 'Hello',
    })
  })

  return router
}
